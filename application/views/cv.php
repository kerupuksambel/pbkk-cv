<html>
    <head>
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Ubuntu:wght@400;700&display=swap" rel="stylesheet"> 
        <link href="assets/fontawesome/css/all.css" rel="stylesheet">
        <link href="assets/css-circle/style.css" rel="stylesheet">
        <link href="assets/style.css" type="text/css" rel="stylesheet">
        <title>Hi. Ammar Alifian here.</title>
    </head>
    <body>
        <div id="btn-up">
            <i class="fas fa-arrow-up"></i>
        </div>
        <header id="intro">
            <h1>Ammar Alifian Fahdan</h1>
            <h3>Undergraduate student, web development enthusiast, sometimes do CTFs and rhythm games.</h3>
        </header>
        <section id="identity">
            <div class="heading">About Me</div>
            <div id="avatar-container">
                <img src="assets/amar_3x4.jpg" id="avatar">
            </div>
            <div id="boxes">
                <div class="box">
                    <b>Name</b>
                    <p>Ammar Alifian Fahdan</p>
                </div>
                <div class="box">
                    <b>Age</b>
                    <p>20</p>
                </div>
                <div class="box">
                    <b>Address</b>
                    <p>Perumdos Blok U-43, Keputih, Sukolilo, Surabaya</p>
                </div>
            </div>
        </section>
        <section id="skills">
            <div class="heading">Skills</div>
            <div class="container">
                <div class="skill">
                    <div class="skill-icon">
                        <i class="fab fa-php"></i>
                    </div>
                    <div class="skill-label">PHP</div>
                </div>
                <div class="skill">
                    <div class="skill-icon">
                        <i class="fab fa-laravel"></i>
                    </div>
                    <div class="skill-label">Laravel</div>
                </div>
                <div class="skill">
                    <div class="skill-icon">
                        <i class="fas fa-fire"></i>
                    </div>
                    <div class="skill-label">CodeIgniter</div>
                </div>
                <div class="skill">
                    <div class="skill-icon">
                        <i class="fab fa-bootstrap"></i>
                    </div>
                    <div class="skill-label">Bootstrap</div>
                </div>
                <div class="skill">
                    <div class="skill-icon">
                        <i class="fab fa-react"></i>
                    </div>
                    <div class="skill-label">React</div>
                </div>
                <div class="skill">
                    <div class="skill-icon">
                        <i class="fab fa-python"></i>
                    </div>
                    <div class="skill-label">Python</div>
                </div>
            </div>
        </section>
        <section id="education">
            <div class="heading">Education</div>
            <div class="container">
                <div id="bar-main"></div>
                <div id="bar-node-container">
                    <div class="bar-node">
                        <b>2012</b>
                        <p>SDI Al-Badar</p>
                    </div>
                    <div class="bar-node">
                        <b>2012 - 2015</b>
                        <p>SMPN 1 Tulungagung</p>
                    </div>
                    <div class="bar-node">
                        <b>2015  -2018</b>
                        <p>SMAN 1 Trenggalek</p>
                    </div>
                    <div class="bar-node">
                        <b>2018  - ...</b>
                        <p>Institut Teknologi Sepuluh Nopember</p>
                    </div>
                </div>
            </div>
        </section>
        <section id="portfolio">
            <div class="heading">Portfolio</div>
            <div class="container">
                <div class="port-container" data-port-id="1" onclick="showImg(1)">
                    <div class="port-img">
                        <img src="assets/img/portfolio/1.jpg"/>
                    </div>
                    <div class="port-caption">Schematics 2019</div>
                </div>
                <div class="port-container" data-port-id="2"  onclick="showImg(2)">
                    <div class="port-img">
                        <img src="assets/img/portfolio/2.png"/>
                    </div>
                    <div class="port-caption">Sistem Informasi Reservasi Restoran</div>
                </div>
                <div class="port-container" data-port-id="3" onclick="showImg(3)">
                    <div class="port-img">
                        <img src="assets/img/portfolio/4.png"/>
                    </div>
                    <div class="port-caption">Sistem Informasi Ujian</div>
                </div>
                <div class="port-container" data-port-id="4" onclick="showImg(4)">
                    <div class="port-img">
                        <img src="assets/img/portfolio/5.png"/>
                    </div>
                    <div class="port-caption">Sistem Informasi Pemesanan Perlengkapan Pesta</div>
                </div>
            </div>
        </section>
        <section id="contact">
            <div class="heading">Contact</div>
            <div class="container">
                <a href="https://facebook.com/ammaralifian">
                    <div class="cont-container">
                        <div class="cont-icon">
                            <i class="fab fa-facebook"></i>
                        </div>
                        <div class="cont-name">Facebook</div>
                        <div class="cont-desc">ammaralifian</div>
                    </div>
                </a>
                <a href="https://gitlab.com/kerupuksambel">
                    <div class="cont-container">
                        <div class="cont-icon">
                            <i class="fab fa-gitlab"></i>
                        </div>
                        <div class="cont-name">Gitlab</div>
                        <div class="cont-desc">kerupuksambel</div>
                    </div>
                </a>
                <a href="https://github.com/kerupuksambel">
                    <div class="cont-container">
                        <div class="cont-icon">
                            <i class="fab fa-github"></i>
                        </div>
                        <div class="cont-name">Github</div>
                        <div class="cont-desc">kerupuksambel</div>
                    </div>
                </a>
                <a href="http://kerupuksambel.com">
                    <div class="cont-container">
                        <div class="cont-icon">
                            <i class="fas fa-globe"></i>
                        </div>
                        <div class="cont-name">Website</div>
                        <div class="cont-desc">kerupuksambel.com</div>
                    </div>
                </a>
            </div>
        </section>
    </body>
    <script type="text/javascript">
    function scrollTo(element) {
        window.scroll({
            behavior: 'smooth',
            left: 0,
            top: element.offsetTop
        });
        }

    document.getElementById("btn-up").addEventListener('click', () => {
        scrollTo(document.getElementsByTagName("header")[0]);
    });

    function showImg(idx){
        for (let index = 1; index <= 4; index++) {
            if (index != idx) {
                var element = document.querySelectorAll("[data-port-id='"+ index +"']")[0];
                element.style.display = 'none';
            }else{
                var element = document.querySelectorAll("[data-port-id='"+ index +"']")[0];
                element.style.width = '100%'; 
                element.setAttribute("onClick", "hideImg(" + index + ")");
            }
            
        }
    }

    function hideImg(idx){
        for (let index = 1; index <= 4; index++) {
            // if (index != idx) {
            //     var element = document.querySelectorAll("[data-port-id='"+ index +"']")[0];
            //     element.style.display = 'none';
            // }else{
                var element = document.querySelectorAll("[data-port-id='"+ index +"']")[0];
                element.style.width = '25%';
                element.style.display = 'block'; 
                element.setAttribute("onClick", "showImg(" + index + ")");
            // } 
        }
    }
    </script>
</html>
